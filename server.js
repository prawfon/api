'use strict'
const express = require("express")//เรียกใช้express
const bodyParser = require("body-parser")
const mongoose = require("mongoose")
mongoose.connect('mongodb://localhost:27017/movie_db')
const cors = require("cors")
const db = mongoose.connection
db.on("error", (err) => {
    console.log(err)
})
db.once("open", () => {
    console.log("db connected")//สั่งสร้างโมเดลobjectของมองโกลdb
    const app = express()//setให้appเรียกฟังชั่นexpress
    const port = 8000
    const Movie = require('./models/movie')
    const User = require('./models/user')
    const jwt = require('jsonwebtoken')
    const hash = require('./hash')
    const cost = 10 //เพิ่มkey
    const key = 'secret'
    // const setUserMw = async (req, res, next) => {
    //     try {
    //         const user = await User
    //             .findOne({ email: 'prawfon@mail.com' })
    //             .exec()
    //         req.user = user
    //         next()
    //     } catch (err) {
    //         console.log(err)
    //         res.status(500).send('no user')
    //     }
    // }
    const checkToken = (req, res, next) => {
        let token = req.get('Authorization') || ''
        if (token.indexOf('Bearer') === -1) {
            return res.sendStatus(401)
        }
        token = token.slice('Bearer'.length)//sliceได้arryใหม่
        try {
            const claims = jwt.verify(token, key)
            req.user = claims
            return next()
        } catch (err) {
            console.log(err)
            return res.sendStatus(401)
        }
    }

    app.use(cors())
    // app.use(setUserMw)
    // parse application/x-www-form-urlencoded
    app.use(bodyParser.urlencoded({ extended: false }))
    // parse application/json
    app.use(bodyParser.json())

    //appคือคำสั่งสร้างpartผ่านserver,functionนี้มีการทำงานแบบasync
    app.get('/movies', async (req, res) => {
        try {
            console.log(req.user)
            const movies = await Movie.find().exec()
            const results = {
                results: movies
            };
            res.json(results)
        } catch (err) {
            console.log(err)
            res.status(500).send('opps! error')
        }
    });
    app.post("/signup", async (req, res) => {
        let { email, password } = req.body;
        try {
          password = await hash.genhash(password, cost);
          await User.create({ email, password });
          res.sendStatus(200);
          console.log(req.body);
        } catch (err) {
          console.log(err);
          res.status(500).send("error");
        }
      });
    app.post("/signin", async (req, res) => {
        let { email, password } = req.body
        try {
            const user = await User.findOne({ email: email }).exec()

            if (!user) {
                return res.status(401).send('email or password is incorrect')
            }
            const correct = await hash.compareHash(password, user.password)
            if (!correct) {
                return res.status(401).send('email or password is incorrect')
            }
            const payload = {
                uid: user._id,
                email: user.email
            }
            const token = jwt.sign(payload, key, { expiresIn: '8h' })
            return res.status(200).send({ token })
        } catch (err) {
            console.log(err)
            res.status(500).send('error')
        }
    })


    app.get('/favorites',checkToken, (req, res) => {
        const results = {
            results: favorites
        };
        res.json(results);
    });
    app.post('/favorites',checkToken,async (req, res) => {
        const {id}=req.body
        try{
         const movie = await Movie.findById(id).exec()
         if(!movie){
             return res.status(400).send('no movie')
         }

        const user = await User.findById(req.user.uid)
        user.favorites.push(movie._id)
        await user.save()
        return res.sendStatus(200)

        }catch (err){
            console.log(err)
            return res.status(500).send('opp! error')
        }
    });
    // app.post('/favorites',checkToken, (req, res) => {
    //     const fav = {
    //         id: ++counter,
    //         title: req.body.title
    //     }
    //     favorites.push(fav)
    //     res.json(fav) //.jsonจะคอล.sendกับ.endให้เรา
    // });
    app.delete("/favorites/:id",checkToken, (req, res) => {
        const id = parseInt(req.params.id, 10)
        const index = favorites.find(fav => fav.id === id)
        if (index === -1) {
            return res.sendStatus(404)
        }
        favorites.splice(index, 1)
        res.sendStatus(200)

    });

    //start server ที่portนี้
    app.listen(8000, () => {
        console.log(`Start at ${port}`)
    })
})

