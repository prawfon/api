
const bcrypt = require('bcrypt-nodejs')


exports.genhash = (password, cost) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(cost, (err, salt) => {
            if (err) {
                reject(err)
            }
            bcrypt.hash(password, salt,null, (err,result)=> {
                if(err) {
                    reject(err)
                }
                resolve(result)
            })
        })
    })

}
exports.compareHash = (password, hash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, hash, (err, result) => {
            if (err) {
                reject(err)
            } else {
                resolve(result)
            }
        })
    })
}