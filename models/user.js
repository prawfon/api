'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const User = new Schema({
  email: String,
  password: String,
  favorites: [{type:Schema.Types.ObjectId,ref:'Movie'}]
})
module.exports = mongoose.model("User",User)